#include "Camera.h"

Camera::Camera(float fovy_deg, int width_, int height_){
	if (fovy_deg <= 0 || width_ <= 0 || height_ <= 0)
		throw std::runtime_error("This camera's fovy/width/height is <= 0.");

	float fovy_rad = TO_RAD(fovy_deg);
	float aspect = width_ / (float)height_;
	c.width = width_;
	c.height = height_;
	halfWidth = width_ / 2.0f;
	halfHeight = height_ / 2.0f;
	c.frustum_height = tan(fovy_rad * 0.5f);
	c.frustum_width = c.frustum_height * aspect;
	setRotation_DEG(0, 0, 0);
}

void Camera::setTranslation(float x, float y, float z){
	t.setTranslation(x, y, z);
	c.pos = (Point)t.getTranslation();
	moved = true;
}

void Camera::setRotation_DEG(float x, float y, float z){
	t.setRotation_RAD(TO_RAD(x), TO_RAD(y), TO_RAD(z));
	c.side = t.getXAxis() * -1;
	c.up = t.getYAxis();
	c.view = t.getZAxis();
	moved = true;
}

void Camera::translateBy(float x, float y, float z){
	t.translateBy(x, y, z);
	c.pos = (Point)t.getTranslation();
	moved = true;
}

void Camera::rotateBy_DEG(float x, float y, float z){
	t.rotateBy_RAD(TO_RAD(x), TO_RAD(y), TO_RAD(z));
	c.side = t.getXAxis() * -1;
	c.up = t.getYAxis();
	c.view = t.getZAxis();
	moved = true;
}

void Camera::generateRays(RayInfo* d_rays) const{
	int totalRays = c.width * c.height;
	initRays<<< ceil(totalRays / BLOCK_SIZE), BLOCK_SIZE >>>(d_rays, c);
}

__global__ void initRays(RayInfo *d_firstRays, CamInfo cam){
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if (i >= cam.width * cam.height) return;

	float y = (int)(i / cam.width);
	float x = i - (y * cam.width);

	x = 2.0f * ((x / cam.width) - 0.5f) * cam.frustum_width;
	y = 2.0f * (0.5f - (y / cam.height)) * cam.frustum_height;

	Vec3 dir = cam.view + (cam.up * y) + (cam.side * x);
	dir.normalize();

	d_firstRays[i].ray.o = cam.pos;
	d_firstRays[i].ray.d = dir;
}