#pragma once
#include <EasyBMP.h>
#include "3DMath.h"

class Image{
private:
	float* color;
	int* iteration;
	unsigned int width;
	unsigned int height;

public:
	Image(unsigned int width_, unsigned int height_);
	~Image();
	void AddColorValue(int x, int y, float r, float g, float b);
	void Reset();
	void PrintToFile(std::string filePath);
};