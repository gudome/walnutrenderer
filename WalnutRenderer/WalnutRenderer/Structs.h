#pragma once
#include "3DMath.h"
#include <stdexcept>

//READ - defined constants are all in Structs.h!
#define BLOCK_SIZE 32.0
#define YAW_SENSITIVITY 0.05f
#define PITCH_SENSITIVITY 0.05f

struct Ray{
	Point o;
	Vec3 d;
};

//RGB8
struct Color{
	unsigned char r = 0;
	unsigned char g = 0;
	unsigned char b = 0;
};

struct RayInfo{
	Ray ray;
	int index;
	Color clr;
};

struct Intersection{
	float t;
	Vec3 normal;
	bool fromOutside;

	//TODO: interpolated UV for texturing.
};
