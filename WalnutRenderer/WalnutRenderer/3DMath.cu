#include "3DMath.h"

__device__ __host__ float Math::clamp(float value, float min, float max){
	if (value < min) value = min;
	else if (value > max) value = max;
	return value;
};

__device__ __host__ Vec3::Vec3(){
	x = 0.f;
	y = 0.f;
	z = 0.f;
}

__device__ __host__ Vec3::Vec3(float x_, float y_, float z_){
	x = x_;
	y = y_;
	z = z_;
}

__device__ __host__ Vec3::Vec3(const Vec3& v){
	x = v.x;
	y = v.y;
	z = v.z;
}

__device__ __host__ Vec3::Vec3(const Point& p){
	x = p.x;
	y = p.y;
	z = p.z;
}
__device__ __host__ Vec3 Vec3::operator*(float f) const{
	Vec3 vOut;
	vOut.x = x * f;
	vOut.y = y * f;
	vOut.z = z * f;
	return vOut;
}

__device__ __host__ Vec3& Vec3::operator*=(float f){
	x *= f;
	y *= f;
	z *= f;
	return *this;
}


__device__ __host__  Vec3 Vec3::operator+(const Vec3& v) const{
	Vec3 vOut;
	vOut.x = x + v.x;
	vOut.y = y + v.y;
	vOut.z = z + v.z;
	return vOut;
}

__device__ __host__ Vec3 Vec3::cross(const Vec3 &v) const{
	Vec3 vOut;
	vOut.x = y*v.z - z*v.y;
	vOut.y = z*v.x - x*v.z;
	vOut.z = x*v.y - y*v.x;
	return vOut;
}

__device__ __host__ float Vec3::dot(const Vec3 &v) const{
	return v.x*x + v.y*y + v.z*z;
}

__device__ __host__ void Vec3::normalize(){
	float w = 1.0f / length();
	x *= w;
	y *= w;
	z *= w;
}

__device__ __host__ float Vec3::length() const{
	return sqrt(lengthSquare());
}

__device__ __host__ float Vec3::lengthSquare() const{
	return x*x + y*y + z*z;
}

////////////////////////////////////////////////////////////////////////////////////////////////

__device__ __host__ Point::Point(){
	x = 0.f;
	y = 0.f;
	z = 0.f;
}

__device__ __host__ Point::Point(float x_, float y_, float z_){
	x = x_;
	y = y_;
	z = z_;
}

__device__ __host__ Point::Point(const Point& p){
	x = p.x;
	y = p.y;
	z = p.z;
}

__device__ __host__ Point::Point(const Vec3& v){
	x = v.x;
	y = v.y;
	z = v.z;
}

__device__ __host__ Point Point::operator+(const Vec3 &v) const{
	Point pOut;
	pOut.x = x + v.x;
	pOut.y = y + v.y;
	pOut.z = z + v.z;
	return pOut;
}

__device__ __host__ Vec3 Point::operator-(const Point &p) const{
	Vec3 vOut;
	vOut.x = x - p.x;
	vOut.y = y - p.y;
	vOut.z = z - p.z;
	return vOut;
}
////////////////////////////////////////////////////////////////////////////////////////////////

TMatrix::TMatrix(const Mat3& mRot, const Vec3 &translate){
	//Rot
	R[0][0] = mRot.R[0][0];
	R[0][1] = mRot.R[0][1];
	R[0][2] = mRot.R[0][2];

	R[1][0] = mRot.R[1][0];
	R[1][1] = mRot.R[1][1];
	R[1][2] = mRot.R[1][2];

	R[2][0] = mRot.R[2][0];
	R[2][1] = mRot.R[2][1];
	R[2][2] = mRot.R[2][2];

	//Transform
	T[0] = translate.x;		T[1] = translate.y;		T[2] = translate.z;

	//Transform part of the inverted version
	T_inv[0] = -(R[0][0] * T[0] + R[1][0] * T[1] + R[2][0] * T[2]);
	T_inv[1] = -(R[0][1] * T[0] + R[1][1] * T[1] + R[2][1] * T[2]);
	T_inv[2] = -(R[0][2] * T[0] + R[1][2] * T[1] + R[2][2] * T[2]);
}

__device__ __host__ void TMatrix::mult(Vec3& v){
	float a = v.x, b = v.y, c = v.z;

	v.x = R[0][0]*a + R[0][1]*b + R[0][2]*c;
	v.y = R[1][0]*a + R[1][1]*b + R[1][2]*c;
	v.z = R[2][0]*a + R[2][1]*b + R[2][2]*c;
}

__device__ __host__ void TMatrix::mult(Point& p){
	float a = p.x, b = p.y, c = p.z;

	p.x = R[0][0]*a + R[0][1]*b + R[0][2]*c + T[0];
	p.y = R[1][0]*a + R[1][1]*b + R[1][2]*c + T[1];
	p.z = R[2][0]*a + R[2][1]*b + R[2][2]*c + T[2];
}

__device__ __host__ void TMatrix::inverseMult(Vec3& v){
	float a = v.x, b = v.y, c = v.z;

	v.x = R[0][0]*a + R[1][0]*b + R[2][0]*c;
	v.y = R[0][1]*a + R[1][1]*b + R[2][1]*c;
	v.z = R[0][2]*a + R[1][2]*b + R[2][2]*c;
}

__device__ __host__ void TMatrix::inverseMult(Point& p){
	float a = p.x, b = p.y, c = p.z;

	p.x = R[0][0]*a + R[1][0]*b + R[2][0]*c + T_inv[0];
	p.y = R[0][1]*a + R[1][1]*b + R[2][1]*c + T_inv[1];
	p.z = R[0][2]*a + R[1][2]*b + R[2][2]*c + T_inv[2];
}

__device__ __host__ void TMatrix::inverseTransposeMult(Vec3& n){
	mult(n);
}

////////////////////////////////////////////////////////////////////////////////////////////////

Mat3::Mat3(){
	R[0][0] = 1;		R[0][1] = 0;		R[0][2] = 0;
	R[1][0] = 0;		R[1][1] = 1;		R[1][2] = 0;
	R[2][0] = 0;		R[2][1] = 0;		R[2][2] = 1;
}

Mat3 Mat3::operator*(const Mat3& mIn){
	Mat3 mOut;
	mOut.R[0][0] = R[0][0]*mIn.R[0][0] + R[0][1]*mIn.R[1][0] + R[0][2]*mIn.R[2][0];		
	mOut.R[0][1] = R[0][0]*mIn.R[0][1] + R[0][1]*mIn.R[1][1] + R[0][2]*mIn.R[2][1];		
	mOut.R[0][2] = R[0][0]*mIn.R[0][2] + R[0][1]*mIn.R[1][2] + R[0][2]*mIn.R[2][2];

	mOut.R[1][0] = R[1][0]*mIn.R[0][0] + R[1][1]*mIn.R[1][0] + R[1][2]*mIn.R[2][0];		
	mOut.R[1][1] = R[1][0]*mIn.R[0][1] + R[1][1]*mIn.R[1][1] + R[1][2]*mIn.R[2][1];		
	mOut.R[1][2] = R[1][0]*mIn.R[0][2] + R[1][1]*mIn.R[1][2] + R[1][2]*mIn.R[2][2];

	mOut.R[2][0] = R[2][0]*mIn.R[0][0] + R[2][1]*mIn.R[1][0] + R[2][2]*mIn.R[2][0];		
	mOut.R[2][1] = R[2][0]*mIn.R[0][1] + R[2][1]*mIn.R[1][1] + R[2][2]*mIn.R[2][1];		
	mOut.R[2][2] = R[2][0]*mIn.R[0][2] + R[2][1]*mIn.R[1][2] + R[2][2]*mIn.R[2][2];

	return mOut;
}

Mat3::Mat3(const Mat3& mIn){
	R[0][0] = mIn.R[0][0];	R[0][1] = mIn.R[0][1];	R[0][2] = mIn.R[0][2];
	R[1][0] = mIn.R[1][0];	R[1][1] = mIn.R[1][1];	R[1][2] = mIn.R[1][2];
	R[2][0] = mIn.R[2][0];	R[2][1] = mIn.R[2][1];	R[2][2] = mIn.R[2][2];
}

Mat3 Mat3::identity(){
	Mat3 m;
	m.R[0][0] = 1;		m.R[0][1] = 0;		m.R[0][2] = 0;
	m.R[1][0] = 0;		m.R[1][1] = 1;		m.R[1][2] = 0;
	m.R[2][0] = 0;		m.R[2][1] = 0;		m.R[2][2] = 1;
	return m;
}

Mat3 Mat3::rotateXMat(float rad){
	Mat3 m;
	m.R[0][0] = 1;		m.R[0][1] = 0;			m.R[0][2] = 0;
	m.R[1][0] = 0;		m.R[1][1] = cos(rad);	m.R[1][2] = -sin(rad);
	m.R[2][0] = 0;		m.R[2][1] = sin(rad);	m.R[2][2] = cos(rad);
	return m;
}


Mat3 Mat3::rotateYMat(float rad){
	Mat3 m;
	m.R[0][0] = cos(rad);		m.R[0][1] = 0;		m.R[0][2] = sin(rad);
	m.R[1][0] = 0;				m.R[1][1] = 1;		m.R[1][2] = 0;
	m.R[2][0] = -sin(rad);		m.R[2][1] = 0;		m.R[2][2] = cos(rad);
	return m;
}

Mat3 Mat3::rotateZMat(float rad){
	Mat3 m;
	m.R[0][0] = cos(rad);	m.R[0][1] = -sin(rad);	m.R[0][2] = 0;
	m.R[1][0] = sin(rad);	m.R[1][1] = cos(rad);	m.R[1][2] = 0;
	m.R[2][0] = 0;			m.R[2][1] = 0;			m.R[2][2] = 1;
	return m;
}

Mat3 Mat3::scaleMat(float scale){
	Mat3 m;
	m.R[0][0] = scale;	m.R[0][1] = 0;		m.R[0][2] = 0;
	m.R[1][0] = 0;		m.R[1][1] = scale;	m.R[1][2] = 0;
	m.R[2][0] = 0;		m.R[2][1] = 0;		m.R[2][2] = scale;
	return m;
}