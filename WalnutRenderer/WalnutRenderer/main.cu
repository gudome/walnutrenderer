/*
	Walnut Renderer - A CUDA Monte Carlo path tracer
	Dev by Dome Pongmongkol.
*/
#include "main.h"

int main(char** args)
{
	cam = new Camera(45, 800, 600);
	renderer = new Renderer(*cam);

	windowInit();
	glInit();
	mainLoop();
	cleanUp();
}

void windowInit(){
	glfwSetErrorCallback(errorCallBack);

	if (!glfwInit()) 
		exit(EXIT_FAILURE);

	window = glfwCreateWindow(cam->getWidth(), cam->getHeight(), "Walnut Renderer", NULL, NULL);

	if (!window){
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, cursorCallback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(window, cam->getHalfWidth(), cam->getHalfHeight());
}

void glInit(){
	//set GL context
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK){
		exit(EXIT_FAILURE);
	}

	initShader();
	initVAO();

	//set the first graphics card.
	cudaSetDevice(0);	

	int pixelSize = cam->getWidth() * cam->getHeight() * sizeof(Color);
	glGenBuffers(1, &pbo);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, pixelSize, NULL, GL_DYNAMIC_COPY);
	cudaGraphicsGLRegisterBuffer(&d_pbo, pbo, cudaGraphicsMapFlagsWriteDiscard);

	glGenTextures(1, &displayImage);
	glBindTexture(GL_TEXTURE_2D, displayImage);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, cam->getWidth(), cam->getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

}

void mainLoop(){
	while (!glfwWindowShouldClose(window)){
		glfwPollEvents();

		//while the resource is mapped, it shouldn't be accessed by any other API.
		cudaGraphicsMapResources(1, &d_pbo, 0);

		Color* pixels;
		size_t size;
		cudaGraphicsResourceGetMappedPointer((void**)&pixels, &size, d_pbo);

		//perform CUDA calculation!
		renderer->render(pixels);

		std::string windowName = "Walnut Renderer | Iteration: ";
		windowName += std::to_string(renderer->getIteration());
		glfwSetWindowTitle(window, windowName.c_str());

		cudaGraphicsUnmapResources(1, &d_pbo, 0);

		//load data from pbo and feed it into the texture.
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
		glBindTexture(GL_TEXTURE_2D, displayImage);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, cam->getWidth(), cam->getHeight(), GL_RGB, GL_UNSIGNED_BYTE, NULL);

		//render
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
		glfwSwapBuffers(window);
	}
}


void cleanUp(){
	glfwDestroyWindow(window);
	glfwTerminate();
}

void initShader(){
	GLuint program = glCreateProgram();
	
	GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
	GLint vslen = VSsource.length();
	const char* vs = &VSsource[0];
	glShaderSource(vertex, 1, &vs, &vslen);
	glCompileShader(vertex);

	GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
	GLint fslen = FSSource.length();
	const char* fs = &FSSource[0];
	glShaderSource(fragment, 1, &fs, &fslen);
	glCompileShader(fragment);

	glAttachShader(program, vertex);
	glAttachShader(program, fragment);
	glLinkProgram(program);

	glUseProgram(program);
}

//init the vao (basically a "canvas" square for rendering.)
void initVAO() {
	GLfloat vertices[] = {
		-1.0f, -1.0f,
		1.0f, -1.0f,
		1.0f, 1.0f,
		-1.0f, 1.0f,
	};

	GLfloat texcoords[] = {
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f
	};

	GLushort indices[] = { 0, 1, 3, 3, 1, 2 };

	GLuint vbo[3];
	GLuint posLocation = 0;
	GLuint texLocation = 1;

	glGenBuffers(3, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(posLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(posLocation);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoords), texcoords, GL_STATIC_DRAW);
	glVertexAttribPointer(texLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(texLocation);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[2]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
}


///////////////////////////////////////////////////////////////////////////////////////

//GLFW fires this method when it fails.
void errorCallBack(int error, const char* description){
	fputs(description, stderr);
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		exit(0);
	//else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
		//TODO: print image
}

void cursorCallback(GLFWwindow* window, double xpos, double ypos){
	//camera control
	float yaw = (xpos - cam->getHalfWidth()) * YAW_SENSITIVITY;
	float pitch = (ypos - cam->getHalfHeight()) * PITCH_SENSITIVITY;

	cam->rotateBy_DEG(pitch, yaw, 0);

	//reset cursor to middle pos.
	glfwSetCursorPos(window, cam->getHalfWidth(), cam->getHalfHeight());
}