#include "Renderer.h"

Renderer::Renderer(Camera& c){
	cam = &c;
	iteration = 0;

	cudaMalloc(&d_rayInfo, cam->getSize() * sizeof(RayInfo));
	cudaMalloc(&d_pixels, cam->getSize() * sizeof(Color));

	cam->generateRays(d_rayInfo);
}

void Renderer::render(Color* gl_pixels){
	//if the camera is moved -> reset the rendering.
	if (cam->isMoved()){
		iteration = 0;
		cam->generateRays(d_rayInfo);
		cam->unmoved();
	}

	iterate();
	loadColorToGLBuffer<<<ceil(cam->getSize() / BLOCK_SIZE), BLOCK_SIZE >>>
		(gl_pixels, d_pixels, iteration, cam->getSize());
}

void Renderer::iterate(){
	iteration++;

	//while not all rays in every iteration is shot

	//copy rays to fill in the holes

	//shoot

	//steam compact the ray

	//add color into the Image object
}

__global__ void testVectorClr(Color* d_pixels, RayInfo* d_rayInfo, int imageSize){
	int i = (blockIdx.x * blockDim.x) + threadIdx.x;
	if (i >= imageSize) return;

	Vec3 rd = d_rayInfo[i].ray.d;
	d_pixels[i].r = (unsigned char)((rd.x * 255) < 0 ? 0 : (rd.x * 255));
	d_pixels[i].g = (unsigned char)((rd.y * 255) < 0 ? 0 : (rd.y * 255));
	d_pixels[i].b = (unsigned char)((rd.z * 255) < 0 ? 0 : (rd.z * 255));

}

__global__ void loadColorToGLBuffer(Color* gl_pixels, Color* d_pixels, int iteration, int imageSize){
	int i = (blockIdx.x * blockDim.x) + threadIdx.x;
	if (i >= imageSize) return;
	
	gl_pixels[i] = d_pixels[i];
}