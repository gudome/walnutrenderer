/*
	Walnut Renderer - A CUDA Monte Carlo path tracer
	Dev by Dome Pongmongkol.
*/

#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include "cuda_gl_interop.h"
#include "Camera.h"
#include "Renderer.h"

void windowInit();
void glInit();
void mainLoop();
void cleanUp();

void initShader();
void initVAO();

void errorCallBack(int error, const char* description);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void cursorCallback(GLFWwindow* window, double xpos, double ypos);

Camera* cam;
Renderer* renderer;
GLFWwindow* window;
std::string windowName;

GLuint pbo;
cudaGraphicsResource* d_pbo;
GLuint displayImage;

std::string VSsource =
"	attribute vec4 Position; \n"
"	attribute vec2 Texcoords; \n"
"	varying vec2 v_Texcoords; \n"
"	\n"
"	void main(void){ \n"
"		v_Texcoords = Texcoords; \n"
"		gl_Position = Position; \n"
"	}";

std::string FSSource =
"	varying vec2 v_Texcoords; \n"
"	\n"
"	uniform sampler2D u_Image; \n"
"	\n"
"	void main(void){ \n"
"		gl_FragColor = texture2D(u_Image, v_Texcoords); \n"
"	}";