#pragma once
#include "3DMath.h"

class Transform
{
private:
	Mat3 rotMatrix;
	Vec3 translate;
	float rotX, rotY, rotZ;	//in Radians
	float scale;

	void UpdateRotMatrix();

public:
	Transform();
	void setTranslation(float x, float y, float z);
	void setRotation_RAD(float x, float y, float z);
	void translateBy(float x, float y, float z);
	void rotateBy_RAD(float x, float y, float z);
	void setScale(float s);
	Vec3 getXAxis() const;
	Vec3 getYAxis() const;
	Vec3 getZAxis() const;
	Vec3 getTranslation() const;
	TMatrix GetTMatrix();
};