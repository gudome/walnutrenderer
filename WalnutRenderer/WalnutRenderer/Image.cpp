#include "Image.h"

Image::Image(unsigned int width_, unsigned int height_){
	color = new float[width_ * height_ * 3]();
	iteration = new int[width_ * height_]();
	width = width_;
	height = height_;
}

Image::~Image(){
	delete[] color;
	delete[] iteration;
}

void Image::AddColorValue(int x, int y, float r, float g, float b){
	int bitPtr = (y * width + x) * 3;
	color[bitPtr] = Math::clamp(r, 0, 1);
	color[bitPtr + 1] = Math::clamp(g, 0, 1);
	color[bitPtr + 2] = Math::clamp(b, 0, 1);

	iteration[y * width + x]++;
}

void Image::Reset(){
	int dim = width * height;
	for (int i = 0; i < dim; i++){
		int bitPtr = i * 3;
		color[bitPtr] = 0;
		color[bitPtr + 1] = 0;
		color[bitPtr + 2] = 0;
		iteration[i] = 0;
	}
}

void Image::PrintToFile(std::string filePath){
	BMP output;
	output.SetSize(width, height);
	output.SetBitDepth(24);

	for (unsigned int y = 0; y < height; y++) {
		for (unsigned int x = 0; x < width; x++) {
			int bitPtr = (y * width + x) * 3;
			float muliplier = 255.0f / iteration[y * width + x];

			output(x, y)->Red = color[bitPtr] * muliplier;
			output(x, y)->Green = color[bitPtr + 1] * muliplier;
			output(x, y)->Blue = color[bitPtr + 2] * muliplier;
		}
	}

	std::cout << filePath.c_str() << " made." << std::endl;
	output.WriteToFile(filePath.c_str());
}