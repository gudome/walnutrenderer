#pragma once
#include "Structs.h"

class Shape{
//public:
	__device__ virtual bool Intersects(Intersection &i, Ray &local_ray) = 0;
};

//radius = 0.5
class Sphere : public Shape{
//public:
	__device__ virtual bool Intersects(Intersection &i, Ray &local_ray);
};