#pragma once
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <math.h>

#define TO_RAD(x) (x * 0.01745329251f)
#define TO_DEG(x) (x / 57.2957795131f)

class Math{
public:
	__device__ __host__ static float clamp(float value, float min, float max);
};

class Point;
class Vec3{
public:
	float x;
	float y;
	float z;

	__device__ __host__ Vec3();
	__device__ __host__ Vec3(float x_, float y_, float z_);
	__device__ __host__ Vec3(const Vec3& v);
	__device__ __host__ explicit Vec3(const Point& p);
	__device__ __host__ Vec3 operator*(float f) const;
	__device__ __host__ Vec3& operator*=(float f);
	__device__ __host__ Vec3 operator+(const Vec3& v) const;
	__device__ __host__ Vec3 cross(const Vec3& v) const;
	__device__ __host__ float dot(const Vec3& v) const;
	__device__ __host__ void normalize();
	__device__ __host__ float length() const;
	__device__ __host__ float lengthSquare() const;
};

////////////////////////////////////////////////////////////////////////////////////////////////

class Point{
public:
	float x;
	float y;
	float z;

	__device__ __host__ Point();
	__device__ __host__ Point(float x_, float y_, float z_);
	__device__ __host__ Point(const Point& p);
	__device__ __host__ explicit Point(const Vec3& v);
	__device__ __host__ Point operator+(const Vec3& v) const;
	__device__ __host__ Vec3 operator-(const Point& p) const;
};

////////////////////////////////////////////////////////////////////////////////////////////////

class Mat3{
public:
	float R[3][3];

	Mat3();
	Mat3(const Mat3& mIn);
	Mat3 operator*(const Mat3& mIn);
	static Mat3 identity();
	static Mat3 rotateXMat(float rad);
	static Mat3 rotateYMat(float rad);
	static Mat3 rotateZMat(float rad);
	static Mat3 scaleMat(float scale);
};

////////////////////////////////////////////////////////////////////////////////////////////////
/* 
	Affine Transformation matrix.
	R T
	0 1
*/
class TMatrix{
private:
	float R[3][3];
	float T[3];
	float T_inv[3];

public:
	//constructor -> called by Transform ONLY
	TMatrix(const Mat3& mRot, const Vec3 &translate);

	//object->world
	__device__ __host__ void mult(Vec3& v);
	__device__ __host__ void mult(Point& p);

	//world->object
	__device__ __host__ void inverseMult(Vec3& v);
	__device__ __host__ void inverseMult(Point& p);

	//for normal (object->world)
	__device__ __host__ void inverseTransposeMult(Vec3& n);
};

////////////////////////////////////////////////////////////////////////////////////////////////