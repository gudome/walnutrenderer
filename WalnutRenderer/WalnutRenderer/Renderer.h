#pragma once
#include "Image.h"
#include "Camera.h"

class Renderer{
private:
	Camera* cam;
	Color* d_pixels;
	RayInfo* d_rayInfo;
	int iteration;

	void iterate();

public:
	Renderer(Camera& c);
	void render(Color* gl_pixels);
	int getIteration() { return iteration; }
};

__global__ void testVectorClr(Color* gl_pixels, RayInfo* d_rayInfo, int imageSize);
__global__ void loadColorToGLBuffer (Color* gl_pixels, Color* d_pixels, int iteration, int imageSize);