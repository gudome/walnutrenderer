#include "Shape.h"

__device__ bool Sphere::Intersects(Intersection &i, Ray &r){
	//Ax^2 + By + C = 0
	float A = r.d.x*r.d.x + r.d.y*r.d.y + r.d.z*r.d.z;
	float B = 2 * (r.d.x*r.o.x + r.d.y*r.o.y + r.d.z*r.o.z);
	float C = r.o.x*r.o.x + r.o.y*r.o.y + r.o.z*r.o.z - 0.25f;

	//re-use C to store insideRoot to reduce register #
	C = B*B - 4 * A*C;
	if (C < 0){
		i.t = -1;
		return false;
	}

	C = sqrtf(C);
	
	A = 1.0f / (2 * A);
	float t0 = (-B + C) * A;
	float t1 = (-B - C) * A;

	//no intersection
	if (t0 < 0 && t1 < 0) {	
		i.t = -1;
		return false;
	}
	//two intersections, pick nearest t
	else if (t0 > 0 && t1 > 0) {
		i.t = t0 < t1 ? t0 : t1;
		i.fromOutside = true;
	}
	//one intersection, the ray must be originated from inside.
	else {	
		i.t = t0 > t1 ? t0 : t1;
		i.fromOutside = false;
	}

	//calculate normal
	Vec3 v = Vec3(r.o + r.d * i.t);
	v.normalize();
	i.normal = v;

	return true;
}