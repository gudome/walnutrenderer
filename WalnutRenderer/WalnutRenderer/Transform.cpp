#include "Transform.h"

Transform::Transform()
{
	rotMatrix = Mat3::identity();
	translate = Vec3(0, 0, 0);
	rotX = 0;
	rotY = 0;
	rotZ = 0;
	scale = 1;
}

//z-y-x rot order
void Transform::UpdateRotMatrix()
{
	rotMatrix = Mat3::scaleMat(scale) * 
				Mat3::rotateXMat(rotX) *
				Mat3::rotateYMat(rotY) *
				Mat3::rotateZMat(rotZ);
}

void Transform::setTranslation(float x, float y, float z){
	translate.x = x;
	translate.y = y;
	translate.z = z;
}

void Transform::setRotation_RAD(float x, float y, float z){
	rotX = x;
	rotY = y;
	rotZ = z;
	UpdateRotMatrix();
}

void Transform::translateBy(float x, float y, float z){
	translate.x += x;
	translate.y += y;
	translate.z += z;
}

void Transform::rotateBy_RAD(float x, float y, float z){
	rotX += x;
	rotY += y;
	rotZ += z;
	UpdateRotMatrix();
}

void Transform::setScale(float s){
	scale = s;
	UpdateRotMatrix();
}

Vec3 Transform::getXAxis() const{
	return Vec3(rotMatrix.R[0][0], rotMatrix.R[1][0], rotMatrix.R[2][0]);
}

Vec3 Transform::getYAxis() const{
	return Vec3(rotMatrix.R[0][1], rotMatrix.R[1][1], rotMatrix.R[2][1]);
}

Vec3 Transform::getZAxis() const{
	return Vec3(rotMatrix.R[0][2], rotMatrix.R[1][2], rotMatrix.R[2][2]);
}

Vec3 Transform::getTranslation() const{
	return translate;
}

TMatrix Transform::GetTMatrix()
{
	return TMatrix(rotMatrix, translate);
}