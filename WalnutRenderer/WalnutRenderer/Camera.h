#pragma once
#include "Structs.h"
#include "Transform.h"

//all vectors must be normalized
//This will be sent to GPU's constant mem thru args.
struct CamInfo{
	Point pos;
	Vec3 up;
	Vec3 view;
	Vec3 side;
	int width;
	int height;
	float frustum_width; //for converting NDC -> 3D coord
	float frustum_height;
};

class Camera{
private:
	CamInfo c;
	Transform t;
	int halfWidth;	//for camera control
	int halfHeight;
	bool moved;

public:
	Camera(float fovy_deg, int width, int height);

	//setters
	void setTranslation(float x, float y, float z);
	void setRotation_DEG(float x, float y, float z);
	void translateBy(float x, float y, float z);
	void rotateBy_DEG(float x, float y, float z);
	void unmoved() { moved = false; }

	//getters
	int getSize() const { return c.width * c.height; }
	int getWidth() const { return c.width; }
	int getHeight() const { return c.height; }
	int getHalfWidth() const { return halfWidth; }
	int getHalfHeight() const { return halfHeight; }
	bool isMoved() const { return moved; }

	void generateRays(RayInfo* d_rays) const;
};

__global__ void initRays(RayInfo* d_firstRays, CamInfo cam);